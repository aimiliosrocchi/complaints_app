package com.mpsp21007.smartalert;

public class User {
    public String name, email;

    public User(){}

    public User(String name, String email){
        this.name = name;
        this.email = email;
    }
}
