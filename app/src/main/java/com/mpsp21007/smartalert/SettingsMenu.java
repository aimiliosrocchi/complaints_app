package com.mpsp21007.smartalert;


import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class SettingsMenu extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button button1, button2, button3;
    Spinner responseSpinner1, responseSpinner2;
    TextView textView1, textView2, textView3, textView4, textView5;

    String contactNumber, chosenRingtone, response_mode, language, lang;
    long responseTime;
    Ringtone ringtone;
    int count;
    TextView text, setHeading;
    final long[] spinnerValues = {60000, 30000, 15000};
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_menu);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        lang = sharedpreferences.getString("Language", "");
        if (lang.equals("null") || lang.isEmpty()) {
            lang = "English";
        }

        button1 = (Button) findViewById(R.id.chosContact);
        button2 = (Button) findViewById(R.id.chosSound);
        button3 = (Button) findViewById(R.id.saveSettings);
        textView1 = (TextView) findViewById(R.id.setmenu1);
        textView2 = (TextView) findViewById(R.id.setmenu2);
        textView3 = (TextView) findViewById(R.id.setmenu3);
        textView4 = (TextView) findViewById(R.id.setmenu4);
        textView5 = (TextView) findViewById(R.id.setmenu5);
        responseSpinner1 = (Spinner) findViewById(R.id.response_spinner);
        responseSpinner2 = (Spinner) findViewById((R.id.response_spinner2));

        languageCheck();
        //Contact Button
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Recipient Contact selection for the Alerts
                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 4);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            //Choose Sound Button
            @Override
            public void onClick(View v) {   //Ringtone for Fall and  Earthquake Alerts
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                startActivityForResult(intent, 5);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            //Save
            @Override
            public void onClick(View v) {
                //Saving the Settings, all options are required
                boolean flag;
                if ((chosenRingtone == null) || (contactNumber == null) || (response_mode == null) || (language == null)) {
                    if (lang.equals("Ελληνικά")) {
                        Toast.makeText(getApplicationContext(), "Παρακαλώ συμπληρώστε όλες τις ρυθμίσεις", Toast.LENGTH_LONG).show();
                    } else if (lang.equals("English")) {
                        Toast.makeText(getApplicationContext(), "Please fill all Settings", Toast.LENGTH_LONG).show();
                    } else if (lang.equals("Italian")) {

                    }

                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();  //Save settings to be shared
                    editor.clear();
                    editor.putString("Contact_number", contactNumber);
                    editor.putString("Ringtone", chosenRingtone);
                    editor.putLong("Response", responseTime);
                    editor.putString("Language", language);
                    flag = editor.commit();
                    if (flag == true) {
                        if (lang.equals("Ελληνικά")) {
                            Toast.makeText(getApplicationContext(), "Οι Ρυθμίσεις Αποθηκεύτηκαν", Toast.LENGTH_LONG).show();
                        } else if (lang.equals("English")) {
                            Toast.makeText(getApplicationContext(), "Settings Saved", Toast.LENGTH_LONG).show();
                        } else if (lang.equals("Italian")) {
                            Toast.makeText(getApplicationContext(), "Impostazioni Salvate", Toast.LENGTH_LONG).show();
                        }
                        Intent intent = new Intent(SettingsMenu.this, Menu.class); //going to Menu activity
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }

            }
        });

        responseSpinner1.setOnItemSelectedListener(this);
        List<String> responseTime = new ArrayList<String>();
        // Fill the response time dropdown for each language
        if (lang.equals("Ελληνικά")) {
            responseTime.add("1 λεπτό");
            responseTime.add("30 δευτερόλεπτα");
            responseTime.add("15 δευτερόλεπτα");
        } else if (lang.equals("English")) {
            responseTime.add("1 minute");
            responseTime.add("30 seconds");
            responseTime.add("15 seconds");
        } else if (lang.equals("Italian")) {
            responseTime.add("1 minuto");
            responseTime.add("30 secondi");
            responseTime.add("15 secondi");
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, responseTime);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        responseSpinner1.setAdapter(dataAdapter);

        responseSpinner2.setOnItemSelectedListener(this);
        //Fill dd with the available languages
        List<String> categories2 = new ArrayList<String>();
        categories2.add("English");
        categories2.add("Ελληνικά");
        categories2.add("Italian");

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories2);

        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        responseSpinner2.setAdapter(dataAdapter2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //get results from the contact and ringtone selected

        if (resultCode == Activity.RESULT_OK && requestCode == 5) {
            Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null) {
                this.chosenRingtone = uri.toString();
            } else {
                this.chosenRingtone = null;
            }
            Toast.makeText(this, chosenRingtone, Toast.LENGTH_LONG).show();
        } else if (resultCode == Activity.RESULT_OK && requestCode == 4) {
            Uri phone_uri = data.getData();
            // get data for sms receiver
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

            Cursor cursor = getContentResolver().query(phone_uri, projection,
                    null, null, null);
            cursor.moveToFirst();

            int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            contactNumber = cursor.getString(numberColumnIndex);

            int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String name = cursor.getString(nameColumnIndex);
        } else {
            if (lang.equals("Ελληνικά")) {
                Toast.makeText(this, "Κανένα Αποτέλεσμα", Toast.LENGTH_LONG).show();
            } else if (lang.equals("English")) {
                Toast.makeText(this, "No Results", Toast.LENGTH_LONG).show();
            } else if (lang.equals("Italian")) {
                Toast.makeText(this, "Nessun Risultato", Toast.LENGTH_LONG).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {  //dropdown spinners (response time and language)
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.response_spinner) {
            response_mode = responseSpinner1.getItemAtPosition(position).toString();
            responseTime = spinnerValues[position];
        } else if (spinner.getId() == R.id.response_spinner2) {
            language = responseSpinner2.getSelectedItem().toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        languageCheck();
    }


    private void languageCheck()
    //UI Language Management for greek, english and italian
    {
//        check the selected language
        if (lang.equals("Ελληνικά")) {
            textView1.setText("Ρυθμίσεις");
            textView2.setText("Επιλέξτε την επαφή παραλήπτη των ειδοποιήσεων SMS:");
            textView3.setText("Επιλέξτε τον ήχο ειδοποίησης:");
            textView4.setText("Επιλέξτε τον διαθέσιμο χρόνο απόκρισης του χρήστη:");
            textView5.setText("Επιλέξτε τη γλώσσα της εφαρμογής:");
            button1.setText("Επιλογη Επαφης");
            button2.setText("Επιλογη Ηχου");
            button3.setText("Αποθηκευση Ρυθμισεων");
        } else if (lang.equals("English")) {
            textView1.setText("Settings");
            textView2.setText("Select the SMS Alerts recipient contact:");
            textView3.setText("Select the Alert Sound:");
            textView4.setText("Select the user's available response time:");
            textView5.setText("Select the app's language:");
            button1.setText("Choose Contact");
            button2.setText("Choose Sound");
            button3.setText("Save Settings");
        } else if (lang.equals("Italian")) {
            textView1.setText("Impostazioni");
            textView2.setText("Seleziona il contatto del destinatario degli avvisi SMS:");
            textView3.setText("Seleziona il suono di avviso:");
            textView4.setText("Seleziona il tempo di risposta disponibile dell'utente:");
            textView5.setText("Seleziona la lingua dell'app:");
            button1.setText("Scegli Contatto");
            button2.setText("Scegli Suono");
            button3.setText("Salva le impostazioni");
        }
    }
}
