package com.mpsp21007.smartalert;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 Change Password Functionality
 */
public class ChangePass extends AppCompatActivity {

    EditText e1, e2, e3, e4;
    TextView t1, t2, t3, t4, t5;
    Button b1;
    dbHelper db;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        lang = sharedpreferences.getString("Language", "");
        if (lang.equals("null") || lang.isEmpty()) {
            lang = "English";
        }

        db = new dbHelper(this);
        e1 = (EditText) findViewById(R.id.unameCP);
        e2 = (EditText) findViewById(R.id.oldpass);
        e3 = (EditText) findViewById(R.id.newpass);
        e4 = (EditText) findViewById(R.id.cnewpass);
        b1 = (Button) findViewById(R.id.chgpass);
        t1 = (TextView) findViewById(R.id.change1);
        t2 = (TextView) findViewById(R.id.change2);
        t3 = (TextView) findViewById(R.id.change3);
        t4 = (TextView) findViewById(R.id.change4);
        t5 = (TextView) findViewById(R.id.change5);

        languageCheck();

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            //Password Change Checks - validity of credentials and if passwords match
            public void onClick(View v) {
                String s1 = e1.getText().toString().trim();
                String s2 = e2.getText().toString().trim();
                String s3 = e3.getText().toString().trim();
                String s4 = e4.getText().toString().trim();
                Boolean validcreds = db.validcreds(s1, s2);

                if (s1.equals("") || s2.equals("") || s3.equals("") || s4.equals("")) {
                    if (lang.equals("Ελληνικά")) {
                        Toast.makeText(getApplicationContext(), "Συμπληρώστε τα κενά πεδία.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("English")) {
                        Toast.makeText(getApplicationContext(), "Please fill the empty fields.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("Italian")) {
                        Toast.makeText(getApplicationContext(), "Si prega di compilare i campi vuoti.", Toast.LENGTH_SHORT).show();
                    }
                } else if (validcreds == false) {
                    if (lang.equals("Ελληνικά")) {
                        Toast.makeText(getApplicationContext(), "Τα στοιχεία του τρέχοντος λογαριασμού σας είναι λανθασμένα. Παρακαλώ προσπαθήστε ξανά.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("English")) {
                        Toast.makeText(getApplicationContext(), "The credentials of your current account are wrong. Please try again.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("Italian")) {
                        Toast.makeText(getApplicationContext(), "Le credenziali del tuo conto corrente sono errate. Per favore riprova.", Toast.LENGTH_SHORT).show();
                    }

                } else if (s3.equals(s4)) {
                    Boolean changePass = db.changePass(s1, s2, s3);
                    if (changePass == true) {
                        if (lang.equals("Ελληνικά")) {
                            Toast.makeText(getApplicationContext(), "Η αλλαγή κωδικού πρόσβασης ήταν επιτυχής.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("English")) {
                            Toast.makeText(getApplicationContext(), "Password Change Successful.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("Italian")) {
                            Toast.makeText(getApplicationContext(), "Modifica password riuscita.", Toast.LENGTH_SHORT).show();
                        }
                        Intent i = new Intent(ChangePass.this, Login.class); //going to the login activity
                        startActivity(i);
                    } else {
                        if (lang.equals("Ελληνικά")) {
                            Toast.makeText(getApplicationContext(), "Η αλλαγή κωδικού πρόσβασης απέτυχε.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("English")) {
                            Toast.makeText(getApplicationContext(), "Password Change Failed.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("Italian")) {
                            Toast.makeText(getApplicationContext(), "Modifica della password non riuscita.", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if (lang.equals("Ελληνικά")) {
                        Toast.makeText(getApplicationContext(), "Οι νέοι κωδικοί πρόσβασης δεν ταιριάζουν.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("English")) {
                        Toast.makeText(getApplicationContext(), "New passwords do not match.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("Italian")) {
                        Toast.makeText(getApplicationContext(), "Le nuove password non corrispondono.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        //Checking the language
        super.onResume();
        languageCheck();
    }

    private void languageCheck()
    //UI Language Management
    {
        if (lang.equals("Ελληνικά")) {
            t1.setText("Για να αλλάξετε τον κωδικό πρόσβασής σας, συμπληρώστε τα στοιχεία του τρέχοντος λογαριασμού σας και τον νέο κωδικό πρόσβασης που θέλετε να ορίσετε.");
            t2.setText("Όνομα χρήστη:");
            t3.setText("Παλαιός κωδικός πρόσβασης:");
            t4.setText("Νέος κωδικός πρόσβασης:");
            t5.setText("Επιβεβαίωση νέου κωδικού πρόσβασης:");
            e1.setHint("πληκτρολογήστε το όνομα χρήστη εδώ");
            e2.setHint("πληκτρολογήστε τον παλιό κωδικό πρόσβασης εδώ");
            e3.setHint("πληκτρολογήστε τον νέο κωδικό πρόσβασης εδώ");
            e4.setHint("πληκτρολογήστε ξανά τον νέο κωδικό πρόσβασης εδώ");
            b1.setText("Αλλαγη του κωδικου προσβασης");
        } else if (lang.equals("English")) {
            t1.setText("To change your password, please fill in your current account's credentials and the new password you wish to set.");
            t2.setText("Username:");
            t3.setText("Old Password:");
            t4.setText("New Password:");
            t5.setText("Confirm New Password:");
            e1.setHint("type your username here");
            e2.setHint("type your old password here");
            e3.setHint("type your new password here");
            e4.setHint("retype your new password here");
            b1.setText("Change Your Password");
        } else if (lang.equals("Italian")) {
            t1.setText("Per modificare la password, inserisci le credenziali del tuo conto corrente e la nuova password che desideri impostare.");
            t2.setText("Nome utente:");
            t3.setText("Vecchia password:");
            t4.setText("Nuova password:");
            t5.setText("Conferma la nuova password:");
            e1.setHint("digita qui il tuo nome utente");
            e2.setHint("digita qui la tua vecchia password");
            e3.setHint("digita qui la tua nuova password");
            e4.setHint("ridigita qui la tua nuova password");
            b1.setText("Cambia la tua password");
        }
    }
}
