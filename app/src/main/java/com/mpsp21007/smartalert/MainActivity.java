package com.mpsp21007.smartalert;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    dbHelper db;
    EditText editText1, editText2, editText3, editText4;
    Button button1, button2;
    TextView textView1, textView2, textView3, textView4, textView5;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private FirebaseAuth mAuth;
    String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        lang = sharedpreferences.getString("Language", "");
        if (lang.equals("null") || lang.isEmpty()) {
            lang = "English";
        }

        db = new dbHelper(this);
        editText4 = (EditText) findViewById(R.id.email);
        editText1 = (EditText) findViewById(R.id.unameL);
        editText2 = (EditText) findViewById(R.id.passL);
        editText3 = (EditText) findViewById(R.id.cpass);
        button1 = (Button) findViewById(R.id.register);
        button2 = (Button) findViewById(R.id.gologin);
        textView1 = (TextView) findViewById(R.id.main1);
        textView2 = (TextView) findViewById(R.id.main2);
        textView3 = (TextView) findViewById(R.id.main3);
        textView4 = (TextView) findViewById(R.id.main4);
        textView5 = (TextView) findViewById(R.id.main5);

        languageCheck();

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Registration Process
                String s4 = editText4.getText().toString().trim();
                String s1 = editText1.getText().toString().trim();
                String s2 = editText2.getText().toString().trim();
                String s3 = editText3.getText().toString().trim();

                if (s1.equals("") || s2.equals("") || s3.equals("") || s4.equals("")) {
                    if (lang.equals("Ελληνικά")) {
                        Toast.makeText(getApplicationContext(), "Συμπληρώστε τα κενά πεδία.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("English")) {
                        Toast.makeText(getApplicationContext(), "Please fill the empty fields.", Toast.LENGTH_SHORT).show();
                    } else if (lang.equals("Italian")) {
                        Toast.makeText(getApplicationContext(), "Si prega di compilare i campi vuoti.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (s2.equals(s3)) {

                        Boolean checkuname = db.checkuname(s1);
                        if (checkuname == true) {
                            Boolean insert = db.insertUser(s1, s2);
                            mAuth.createUserWithEmailAndPassword(s4, s2).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        User user = new User(s1, s4);
                                        FirebaseDatabase.getInstance().getReference("Users")
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()){
                                                            Toast.makeText(MainActivity.this, "User has benn registered successfully, please view your emails to confirm your account", Toast.LENGTH_LONG).show();

                                                            //redirect to login layout
                                                        }else{
                                                            Toast.makeText(MainActivity.this, "Failed to Register! Try again!", Toast.LENGTH_LONG).show();

                                                        }

                                                    }
                                                });
                                    }else{
                                        Toast.makeText(MainActivity.this, "Failed to Register! Try again!", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            if (insert) {
                                if (lang.equals("Ελληνικά")) {
                                    Toast.makeText(getApplicationContext(), "Επιτυχής Εγγραφή.", Toast.LENGTH_SHORT).show();
                                } else if (lang.equals("English")) {
                                    Toast.makeText(getApplicationContext(), "Registration Successful.", Toast.LENGTH_SHORT).show();
                                } else if (lang.equals("Italian")) {
                                    Toast.makeText(getApplicationContext(), "Iscrizione completata correttamente.", Toast.LENGTH_SHORT).show();
                                }
                                Intent i = new Intent(MainActivity.this, Login.class); //going to the login activity
                                startActivity(i);
                            } else {
                                if (lang.equals("Ελληνικά")) {
                                    Toast.makeText(getApplicationContext(), "Η εγγραφή απέτυχε.", Toast.LENGTH_SHORT).show();
                                } else if (lang.equals("English")) {
                                    Toast.makeText(getApplicationContext(), "Registration Failed.", Toast.LENGTH_SHORT).show();
                                } else if (lang.equals("Italian")) {
                                    Toast.makeText(getApplicationContext(), "Registrazione fallita.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            if (lang.equals("Ελληνικά")) {
                                Toast.makeText(getApplicationContext(), "Επιλέξτε άλλο όνομα χρήστη. Το συγκεκριμένο υπάρχει ήδη.", Toast.LENGTH_SHORT).show();
                            } else if (lang.equals("English")) {
                                Toast.makeText(getApplicationContext(), "Please select another username. Current one already exists.", Toast.LENGTH_SHORT).show();
                            } else if (lang.equals("Italian")) {
                                Toast.makeText(getApplicationContext(), "Seleziona un altro nome utente. Quello attuale esiste già.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (lang.equals("Ελληνικά")) {
                            Toast.makeText(getApplicationContext(), "Οι κωδικοί πρόσβασης δεν ταιριάζουν.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("English")) {
                            Toast.makeText(getApplicationContext(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
                        } else if (lang.equals("Italian")) {
                            Toast.makeText(getApplicationContext(), "Le passwords non corrispondono.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Login.class); //going to the login activity
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        languageCheck();
        //Checking the Language
    }

    private void languageCheck()
    //UI Language Management
    {
        if (lang.equals("Ελληνικά")) {
            textView1.setText("Καλώς ορίσατε στην εφαρμογή Smart Alert!");
            textView2.setText("Για να συνεχίσετε, παρακαλώ δημιουργήστε ένα νέο λογαριασμό:");
            textView3.setText("Όνομα Χρήστη:");
            textView4.setText("Κωδικός Πρόσβασης:");
            textView5.setText("Επιβεβαίωση Κωδικού Πρόσβασης:");
            button2.setText("Εγγραφη");
            button2.setText("Εχω Ηδη Λογαριασμο");
            editText1.setHint("πληκτρολογήστε εδώ το όνομα χρήστη");
            editText2.setHint("πληκτρολογήστε εδώ τον κωδικό πρόσβασης");
            editText3.setHint("επαναπληκτρολογήστε εδώ τον κωδικό πρόσβασης");
            editText4.setHint("πληκτρολογήστε εδώ το email σας");
        } else if (lang.equals("English")) {
            textView1.setText("Welcome to the Smart Alert App!");
            textView2.setText("To continue, please create a new account:");
            textView3.setText("Username:");
            textView4.setText("Password:");
            textView5.setText("Confirm Password:");
            button1.setText("Register");
            button2.setText("I Already Have An Account");
            editText1.setHint("type your username here");
            editText2.setHint("type your password here");
            editText3.setHint("retype your password here");
            editText4.setHint("type your email here");

        } else if (lang.equals("Italian")) {
            textView1.setText("Benvenuto nell'app Smart Alert!");
            textView2.setText("Per continuare, crea un nuovo account:");
            textView3.setText("Nome utente:");
            textView4.setText("Parola d'ordine:");
            textView5.setText("Conferma password:");
            button1.setText("Registrati");
            button2.setText("Ho già un account");
            editText1.setHint("digita qui il tuo nome utente");
            editText2.setHint("digita qui la tua password");
            editText3.setHint("ridigita qui la tua password");
            editText4.setHint("type your email here");

        }
    }
}
